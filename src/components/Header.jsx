import React, { Component } from 'react';
import '../assets/scss/components/header.scss';
import StopWatchBuy from './StopWatchBuy';

export default () => (
  <header className="header container-fluid">
    <nav className="row">
      <h1 className="col-7">
        <span className="fa fa-align-left header__menu-button"></span>
        <span className="header__link-name"><a href="/">Teste Front</a></span>
      </h1>
      <StopWatchBuy />
    </nav>
  </header>
);