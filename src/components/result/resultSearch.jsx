import React from 'react';
import '../../assets/scss/components/resultSearch.scss';

export default props => {
  const getFlightDuration = (minutesTotal) => {
    let hours = Math.floor(minutesTotal / 60); //since both are ints, you get an int
    let minutes = minutesTotal % 60;
    
    if(minutes === 0){
      minutes = '';
    } else {
      minutes = minutesFormat(minutes);
    }

    return `${hours}H${minutes}`;
  }
  const minutesFormat = (minutes) => {
    return minutes.toString().padStart(2, '0');
  }
  const whereBuy = (percentDicount, mmBestPriceAt, exclusiveMax) => {
    if(exclusiveMax) {
      return (
        <div className="col-12">
          <strong className="price__discount">Exclusivo na Maxmilhas</strong>
        </div>
      )
    }
    if(mmBestPriceAt === 'ota'){
      return (
        <div className="col-12">
          <strong className="price__discount">Mais economia da Cia Aérea</strong>
        </div>
      )
    } else if(mmBestPriceAt === 'miles' && percentDicount !== 0) {
      return (
        <div className="col-12">
          <strong className="price__discount miles">Economize {percentDicount}% na MaxMilhas</strong>
        </div>
      )
    }
  }
  const verifyObjectAirlinePricingExist = (airlineName, airlineDiscount) => {
    if(!airlineDiscount) {
      return false;
    }
    return (
      <div className="col-12 text-uppecase">
        <div className="price__airline">
          {airlineName} <strong>R${airlineDiscount.saleTotal.toLocaleString('pt-BR')} </strong>
        </div>
      </div>
    )
  }

  const price = (flight) => {
    const mmBestPriceAt = flight.pricing.mmBestPriceAt;
    let airlineBestPrice = {};
    let exclusiveMax = false;

    if(mmBestPriceAt === 'ota') {
      airlineBestPrice = flight.pricing.ota;
    } else if(mmBestPriceAt === 'miles') {
      airlineBestPrice = flight.pricing.miles
    } else if(mmBestPriceAt === 'airline') {
      airlineBestPrice = flight.pricing.airline      
    } else {
      airlineBestPrice = flight.pricing.airline
    }
    
    
    if(!flight.pricing.airline && !flight.pricing.ota) {
      exclusiveMax = true;
    }

    return (
      <div className="row price">
        {verifyObjectAirlinePricingExist(flight.pricing.airlineName, flight.pricing.airline)}
        <div className="col-12 text-uppecase">
          <button className="btn btn-primary w-100">R${airlineBestPrice.saleTotal.toLocaleString('pt-BR')}</button>
        </div>
        {whereBuy(Math.round(flight.pricing.savingPercentage), mmBestPriceAt, exclusiveMax)}
      </div>
    )
  }
  const renderRows = () => {
    return props.flightList.map(flight => (
      <div className="table-result__tbody" key={flight.id}>
        <div className="row adjust-padding">
          <div className="col-3 text-uppecase">
            <strong>{flight.airline}</strong>
            <div>{flight.flightNumber}</div>
          </div>
          <div className="col-3 text-uppecase">
            <strong>{new Date(flight.departureDate).getHours()}:{minutesFormat(new Date(flight.departureDate).getMinutes())}</strong>
            <div>{flight.from}</div>
          </div>
          <div className="col-3 text-uppecase">
            <strong>{getFlightDuration(flight.duration)}</strong>
            <div>{
              flight.stops === 0 ? 'VOO Direto' : 
              flight.stops === 1 ? '1 parada' :
              flight.stops +' paradas' }</div>
          </div>
          <div className="col-3 text-uppecase">
          <strong>{new Date(flight.arrivalDate).getHours()}:{minutesFormat(new Date(flight.arrivalDate).getMinutes())}</strong>
            <div>{flight.to}</div>
          </div>
        </div>
        
        {price(flight)}

        <div className="row detail">
          <div className="col-12">
            <i className="fa fa-plus"></i><strong> Detalhes do voo</strong>
          </div>
        </div>
      </div>
    ))
  }

  return (
    <section className="resultSearch">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <div className="resultSearch__alert-price">
              <span>Crie um alerta de preço para essa busca</span>
              <i className="far fa-bell"></i>
            </div>
          </div>
        </div>
        <div className="table-result">
          <div className="table-result__thead">
            <div className="row">
              <div className="col-3"><span>Cia Aérea</span></div>
              <div className="col-3"><span>Partida</span></div>
              <div className="col-3"><span>Duração</span></div>
              <div className="col-3"><span>Chegada</span></div>
            </div>
          </div>
            {renderRows()}
        </div>
      </div>
    </section>
  )
}