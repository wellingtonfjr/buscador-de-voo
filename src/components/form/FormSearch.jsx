import React, { Component } from 'react';
import '../../assets/scss/components/formSearch.scss';
import '../../assets/scss/components/autocomplete.scss';
import Downshift from 'downshift';
import 'flatpickr/dist/themes/material_green.css';

import Flatpickr from 'react-flatpickr';
import { flatpickr } from "flatpickr";
import { Portuguese } from "flatpickr/dist/l10n/pt.js";
import axios from "axios";
import ResultSearch from '../result/ResultSearch';

import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';


export default class FormSearch extends Component {

  constructor(props) {
    super(props);

    this.months = ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
    this.monthsSig = ['JAN','FEV','MAR','ABR','MAI','JUN','JUL','AGO','SET','OUT','NOV','DEZ'];
  
    this.state = {
      // Search
      fromLocation: {
        city: '',
        airport: '',
        sigAirport: '',
        statusShowInput: false
      }, // Origem
      toLocation: {
        city: '',
        airport: '',
        sigAirport: '',
        statusShowInput: false
      }, // Destino
      outboundDate: {
        date: new Date(),
        day: new Date().getDate(),
        month: this.months[new Date().getMonth()],
        monthsSig: this.monthsSig[new Date().getMonth()],
        year: new Date().getFullYear(),
        statusShowInput: false
      }, // Data de partida
      inboundDate: {
        date: null,
        day: null,
        month: null,
        monthsSig: null,
        year: null,
        statusShowInput: false
      }, // Data de volta
      // +{this.state.passenger.statusShowBox}+
      passenger: {
        cabin: 'EC', // Classe econômica (EC) ou executiva (EX)
        adults: 1, // Adultos
        children: 0, // Crianças
        infants: 0, // Bebês
        statusShowBox: false,
        peopleTotal: 1
      },
        
      flightList: [],
      flightListOutbound: {
        flightList: [],
        activeShow: true
      },
      flightListInbound: {
        flightList: [],
        activeShow: false
      }, //
      
      loadingShow: false,

      formSearchShow: true,

      showSearchContainer: true
    };
    
    this.items = [
      {value: 'Brasília', city: 'Brasília',  sigAirport: 'BSB', airport: 'Brasilia'},
      {value: 'Belo Horizonte', city: 'Belo Horizonte', sigAirport: 'CNF', airport: 'Confins Intl'},
    ];

    this.URL_SEARCH_FLIGHTS_API = 'https://flight-price-hmg.maxmilhas.com.br'

  }
  
  createNotificationInfo = (text, timeOut = 3000) => {
    NotificationManager.info(text,'', timeOut);
  };

  createNotificationError = (text, timeOut = 3000) => {
    NotificationManager.error(text,'', timeOut);
    
  }
  
  //Resolver problema de focus
  showAutocomplete = object => {
    object.statusShowInput = true;
    this.setState({...object, statusShowInput: true});
    
  };
  
  blurAutoComplete = object => {
    this.setState({...object, statusShowInput: false});
    object.statusShowInput = false;
  }

  handleChangeDate = (element, dateObject) => {
    dateObject.date = element[0];
    dateObject.day = element[0].getDate();
    dateObject.month = this.months[element[0].getMonth()];
    dateObject.monthsSig = this.monthsSig[element[0].getMonth()];
    dateObject.year = element[0].getFullYear();
    dateObject.statusShowInput = false;
    
    this.setState({
      ...dateObject, dateObject
    });
  }

  showInputOcult = object => {
    object.statusShowInput = true;
    this.setState({
      ...this.state, object
    })
  }
  
  includeZeroForceTwoNumbers = (number) => {
    return number.toString().padStart(2, '0');
  }

  closeModalPassenger = (object) => {
    object.peopleTotal =  parseInt(object.children) +
                          parseInt(object.infants) + 
                          parseInt(object.adults);
    object.statusShowBox = false;
    this.setState({
      ...this.state, object
    })
  }

  handleChangeSelect = (object) => {
    object.statusShowInput = true;
    this.setState({
      ...this.state, object
    })
  }
  runSearch = () => {
    if (this.state.outboundDate.date && this.state.inboundDate.date && 
      this.state.toLocation.sigAirport && this.state.fromLocation.sigAirport){
        
      this.setState({...this.state, showSearchContainer: false, loadingShow: true, flightList: []});

      const mountOutboundDate = `${this.state.outboundDate.year}-${this.includeZeroForceTwoNumbers(this.state.outboundDate.date.getMonth() + 1)}-${this.includeZeroForceTwoNumbers(this.state.outboundDate.day)}`;
      const mountInboundDate = `${this.state.inboundDate.year}-${this.includeZeroForceTwoNumbers(this.state.inboundDate.date.getMonth() + 1)}-${this.includeZeroForceTwoNumbers(this.state.inboundDate.day)}`;

      const postData = {
        tripType: "RT", 
        from: this.state.fromLocation.sigAirport,  //origem
        to: this.state.toLocation.sigAirport,  //destino
        outboundDate: mountOutboundDate, //data de partida
        inboundDate: mountInboundDate, //data de volta
        cabin: this.state.passenger.cabin, //classe econômica (EC) ou executiva (EX)
        adults: this.state.passenger.adults, //adultos
        children: this.state.passenger.children, //crianças
        infants: this.state.passenger.infants, //bebês
      }
      
      const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJtYXhtaWxoYXMuY29tLmJyIiwiaWF0IjoxNTA5MTIwMTAxLCJleHAiOjE1MTA0MTYxMDEsImF1ZCI6InRlc3RlLWZyb250ZW5kIiwic3ViIjoidGVzdGUtZnJvbnRlbmQiLCJlbnYiOiJobWcifQ.nM6wMem6dxF0CcDlig5iA9az5ZfmbXDjq1e4ypZXwjU';
      const config = {
        headers: { Authorization: "bearer " + token }
      }

      let statusAirline = false;
      axios.post(`${this.URL_SEARCH_FLIGHTS_API }/search?time=${Date.now()}`, postData, config)
        .then(resp => {
          const searchId = resp.data.id;
          let closeLoading = false;
          resp.data.airlines.map((airline) => {
            if(airline.status.enable){
              statusAirline = true;
              axios.get(`${this.URL_SEARCH_FLIGHTS_API }/search/${searchId}/flights?airline=${airline.label}`)
              .then(flights => {
                if(!closeLoading){
                  this.setState({...this.state, loadingShow: false, formSearchShow: false});
                  closeLoading = true;
                }

                this.setState( state => {
                  return state.flightList.push(...flights.data.outbound);
                });

                this.setState( state => {
                  return state.flightListOutbound.flightList.push(...flights.data.outbound);
                });

                this.setState( state => {
                  return state.flightListInbound.flightList.push(...flights.data.inbound);
                });
              });
            }
          });
        })
        .finally(() => {
          if(statusAirline === false) {
            this.setState({...this.state, loadingShow: false});
            this.createNotificationError('Nenhum voo encontrado, faça uma nova pesquisa.')
          }
        })
        .catch(error => {
          console.log('error', error.response)
      });;
    } else {
      this.createNotificationError('Todos os campos são obrigatórios');
    }
  }

  render() {
    return (
      <React.Fragment>
        <NotificationContainer/>
        <div className={"loading " + (this.state.loadingShow === true ? 'show-loading' : '')}>
          <svg height="30px" viewBox="0 -101 713.75189 713" width="30px" xmlns="http://www.w3.org/2000/svg"><path d="m85.085938 304.40625c-6.144532 2.882812-8.855469 10.140625-6.117188 16.34375l28.066406 60.25c1.378906 3 3.894532 5.335938 6.988282 6.488281 1.355468.492188 2.792968.746094 4.238281.746094 1.808593.015625 3.59375-.371094 5.238281-1.117187l212.054688-99.046876-19.207032 200.824219c-.371094 3.953125 1.152344 7.839844 4.109375 10.480469 9.296875 7.695312 20.882813 12.078125 32.9375 12.472656 3.324219-.003906 6.628907-.421875 9.851563-1.242187 17.214844-4.492188 30.5625-19.832031 39.667968-46.03125l83.449219-247.101563 165.273438-77.09375c48.648437-22.699218 73.71875-68.726562 56.882812-104.652344-9.554687-18.777343-27.855469-31.550781-48.773437-34.054687-23.242188-3.507813-46.996094.148437-68.105469 10.484375l-190.726563 89.433594-205.0625-80.207032c-25.824218-9.726562-46.277343-9.476562-60.621093.878907-12.035157 9.875-18.882813 24.71875-18.589844 40.285156.125 3.949219 2.113281 7.601563 5.363281 9.859375l138.082032 94.921875-79.078126 37.421875-132.601562-33.683594c-5.433594-1.335937-11.089844 1.113282-13.84375 5.988282l-33.050781 59.496093c-1.714844 3.152344-1.984375 6.894531-.738281 10.261719 1.238281 3.371094 3.871093 6.039062 7.226562 7.328125l98.167969 40.542969zm57.121093-248.09375c.734375-5.273438 3.402344-10.074219 7.488281-13.472656 7.113282-5.117188 20.085938-4.367188 37.421876 2.117187l182.617187 71.226563-83.328125 38.917968zm-112.257812 180.117188 21.703125-38.542969 127.605468 31.679687c2.75.695313 5.652344.433594 8.238282-.742187l414.75-193.71875c16.488281-8.132813 35.058594-11.09375 53.261718-8.484375 12.765626 1.175781 24.136719 8.542968 30.4375 19.707031 10.46875 22.457031-10.109374 55.261719-44.914062 71.476563l-143.695312 67.363281 8.734374-25.828125c2.207032-6.644532-1.398437-13.824219-8.042968-16.027344-6.652344-2.203125-13.828125 1.402344-16.03125 8.046875l-102.660156 305.476563c-6.113282 17.34375-13.722657 27.699218-22.203126 29.9375-5.253906 1.011718-10.691406-.019532-15.21875-2.863282l24.949219-261.953125c.652344-6.886719-4.398437-13-11.292969-13.652343-6.886718-.660157-13 4.398437-13.65625 11.285156l-3.863281 40.039062-214.054687 99.789063-17.578125-37.421875 35.167969-16.460938c4.527343-1.996094 7.445312-6.472656 7.445312-11.414062 0-4.945313-2.917969-9.421875-7.445312-11.414063zm0 0"/></svg>
        </div>
        <section className="resume-search" >
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 resume-search__section resume-search__section--info"
                    role="button"
                    onClick={() => {
                      this.setState({...this.state, showSearchContainer: true})
                    }}>
                <div className="resume-search__items">
                  <i className="fa fa-map-marker-alt"></i>
                  <strong className="resume-search__items__content">{this.state.fromLocation.sigAirport} - {this.state.toLocation.sigAirport}</strong>
                </div>

                <div className="resume-search__items">
                  <i className="far fa-calendar-alt"></i>
                  <span className="resume-search__items__content"><strong>{this.state.outboundDate.day}</strong> {this.state.outboundDate.monthsSig} {this.state.outboundDate.year}</span>
                </div>

                <div className="resume-search__items">
                  <i className="far fa-calendar-alt"></i>
                  <span className="resume-search__items__content"><strong>{this.state.inboundDate.day ? this.state.inboundDate.day : '-' }</strong> {this.state.inboundDate.monthsSig} {this.state.inboundDate.year}</span>
                </div>

                <div className="resume-search__items resume-search__items--passengers">
                  <i className="fas fa-user-friends"></i>
                  <strong 
                    className="resume-search__items__content">{this.state.passenger.peopleTotal}</strong>
                </div>
              </div>

              <div className="resume-search__section resume-search__section--select">
                <button type="button" className={"resume-search__items " + 
                                        (this.state.flightListOutbound.activeShow ? 'active': '') }
                        onClick={() => {
                          
                          this.state.flightListInbound.activeShow = false;
                          this.state.flightListOutbound.activeShow = true;
                          this.setState({
                              ...this.state, 
                              flightList: this.state.flightListOutbound.flightList,
                          })
                          this.setState({...this.state.flightListOutbound, activeShow: true})
                        }}>
                  Selecione sua Ida
                </button>

                <button type="button" className={"resume-search__items " + 
                                        (this.state.flightListInbound.activeShow ? 'active': '')} 
                        onClick={() => {
                          this.state.flightListInbound.activeShow = true;
                          this.state.flightListOutbound.activeShow = false;
                          this.setState({...this.state, flightList: this.state.flightListInbound.flightList})
                        }}>
                  Selecione sua Volta
                </button>
              </div>
            </div>
          </div>
        </section>

        <section className={"container-search " + (this.state.showSearchContainer === true ? 'show-modal' : '')} >
          <form action="" className="form-search">
            <i className="fas fa-times close" 
                onClick={() => {
                  this.setState({...this.state, showSearchContainer: false})
                }}></i>
            <div className="row">
              <div className="col-12">
                <span className="show-modal__title">Pesquise sua passagem.</span>
              </div>

              <div className="col-12">
                <Downshift
                  onChange={selection => {
                      this.setState({ 
                        ...this.state, 
                        fromLocation: { 
                          city: selection.city, 
                          sigAirport: selection.sigAirport, 
                          airport: selection.airport
                        }
                      });
                    }
                  }
                  itemToString={item => (item ? item.value : '')}
                >
                  {({
                    getInputProps,
                    getItemProps,
                    getLabelProps,
                    getMenuProps,
                    isOpen,
                    inputValue,
                    highlightedIndex,
                    selectedItem,
                  }) => (
                    <div className={"form-group input-ocult autocomplete " + (this.state.fromLocation.statusShowInput ? 'show-input' : '')}
                          role="button"
                          aria-label="Selecione o local para onde vai"
                          onClick={
                            () => {
                              this.myInp.focus();
                              this.showAutocomplete(this.state.fromLocation);
                              
                            }
                          }>
                      <label className="form-label" {...getLabelProps()}>Sair de</label>
                      <div className="choice-group-show">
                        <div className="choice-group-show__name">{this.state.fromLocation.city}</div>
                        <div className="choice-group-show__detail">
                          <span className="choice-group-show__detail__sig-airport">{this.state.fromLocation.sigAirport}</span>
                          <span className="choice-group-show__detail__airport">{this.state.fromLocation.airport}</span>
                        </div>
                        <i className="choice-group-show__icon fa fa-map-marker-alt"></i>

                      </div>
                      <input value={this.state.fromLocation} 
                              ref={(ip) => this.myInp = ip}
                              id="fromLocation" placeholder="Cidade ou Aeroporto" 
                              className="form-control" {...getInputProps()} 
                              onBlur={() => this.blurAutoComplete(this.state.fromLocation)} />
                      <ul {...getMenuProps()}>
                        {isOpen
                          ? this.items
                              .filter(item => !inputValue || item.value.includes(inputValue))
                              .map((item, index) => (
                                <li
                                  {...getItemProps({
                                    key: item.value,
                                    index,
                                    item,
                                    style: {
                                      backgroundColor:
                                        highlightedIndex === index ? 'lightgray' : 'white',
                                      fontWeight: selectedItem === item ? 'bold' : 'normal',
                                    },
                                  })}
                                >
                                  <i className="fas fa-plane-departure"></i>
                                  {item.value}
                                </li>
                              ))
                          : null}
                      </ul>
                    </div>
                  )}
                </Downshift>
              </div>

              <div className="col-12">
                <Downshift
                  onChange={selection => {
                      this.setState({
                        ...this.state, 
                        toLocation: {
                          city: selection.city, 
                          sigAirport: selection.sigAirport, 
                          airport: selection.airport
                        }
                      });
                    }
                  }
                  itemToString={item => (item ? item.value : '')}
                >
                  {({
                    getInputProps,
                    getItemProps,
                    getLabelProps,
                    getMenuProps,
                    isOpen,
                    inputValue,
                    highlightedIndex,
                    selectedItem,
                  }) => (
                    <div className={"form-group input-ocult autocomplete " + (this.state.toLocation.statusShowInput ? 'show-input' : '')}
                          role="button"
                          aria-label="Selecione o local para onde vai"
                          onClick={
                            () => {
                              this.myInp.focus();
                              this.showAutocomplete(this.state.toLocation);
                              
                            }
                          }>
                      <label className="form-label" {...getLabelProps()}>Ir para</label>
                      <div className="choice-group-show">
                        <div className="choice-group-show__name">{this.state.toLocation.city}</div>
                        <div className="choice-group-show__detail">
                          <span className="choice-group-show__detail__sig-airport">{this.state.toLocation.sigAirport}</span>
                          <span className="choice-group-show__detail__airport">{this.state.toLocation.airport}</span>
                        </div>
                        <i className="choice-group-show__icon fa fa-map-marker-alt"></i>

                      </div>
                      <input value={this.state.toLocation} 
                              ref={(ip) => this.myInp = ip}
                              id="fromLocation" placeholder="Cidade ou Aeroporto" 
                              className="form-control" {...getInputProps()} 
                              onBlur={() => this.blurAutoComplete(this.state.toLocation)} />
                      <ul {...getMenuProps()}>
                        {isOpen
                          ? this.items
                              .filter(item => !inputValue || item.value.includes(inputValue))
                              .map((item, index) => (
                                <li
                                  {...getItemProps({
                                    key: item.value,
                                    index,
                                    item,
                                    style: {
                                      backgroundColor:
                                        highlightedIndex === index ? 'lightgray' : 'white',
                                      fontWeight: selectedItem === item ? 'bold' : 'normal',
                                    },
                                  })}
                                >
                                  <i className="fas fa-plane-departure"></i>
                                  {item.value}
                                </li>
                              ))
                          : null}
                      </ul>
                    </div>
                  )}
                </Downshift>
              </div>

              <div className="col-6">
                <div className={"form-group input-ocult " + (this.state.outboundDate.statusShowInput ? 'show-input' : '')}
                      role="button"
                      aria-label="Selecione o local para onde vai"
                      onClick={
                        () => {
                          this.showInputOcult(this.state.outboundDate);
                        }
                      }>
                  <label className="form-label" >Ida</label>
                  <div className="choice-group-show">
                    <div className="choice-group-show__name">{this.state.outboundDate.day}</div>
                    <div className="choice-group-show__detail">
                      <span className="choice-group-show__detail__sig-airport">{this.state.outboundDate.month}</span>
                      <span className="choice-group-show__detail__airport">{this.state.outboundDate.year}</span>
                    </div>
                    <i className="choice-group-show__icon far fa-calendar-alt"></i>
                  </div>
                  <Flatpickr 
                    data-enable-time
                    options={
                      {minDate: new Date(),
                      language: Portuguese}
                    }
                    value={this.state.outboundDate.date}
                    onChange={el => this.handleChangeDate(el, this.state.outboundDate)} 
                    onBlur={() => this.blurAutoComplete(this.state.outboundDate)} />
                </div>
              </div>

              <div className="col-6">
                <div className={"form-group input-ocult " + (this.state.inboundDate.statusShowInput ? 'show-input' : '')}
                      role="button"
                      aria-label="Selecione o local para onde vai"
                      onClick={() => {
                          this.showInputOcult(this.state.inboundDate);
                        }
                      }>
                  <label className="form-label" >Volta</label>
                  <div className="choice-group-show">
                    <div className="choice-group-show__name">{this.state.inboundDate.day ? this.state.inboundDate.day : '-'}</div>
                    <div className="choice-group-show__detail">
                      <span className="choice-group-show__detail__sig-airport">{this.state.inboundDate.month ? this.state.inboundDate.month : '-'}</span>
                      <span className="choice-group-show__detail__airport">{this.state.inboundDate.year ? this.state.inboundDate.year : '-'}</span>
                    </div>
                    <i className="choice-group-show__icon far fa-calendar-alt"></i>
                  </div>
                  <Flatpickr 
                    data-enable-time
                    options={
                      {minDate: this.state.outboundDate.date,
                      language: Portuguese}
                    }
                    value={this.state.inboundDate.date}
                    onChange={el => this.handleChangeDate(el, this.state.inboundDate)} 
                    onBlur={() => this.blurAutoComplete(this.state.inboundDate)} />
                </div>
              </div>

              <div className="col-12">

                <div className="form-group box-passenger" 
                      >
                  <label className="form-label" 
                        role="button"
                        aria-label="Passageiros"
                        onClick={() => {
                          this.setState(prevState => ({
                            passenger: {
                              ...prevState.passenger,
                              statusShowBox: true
                            }
                          }));
                          console.log(this.state);
                        }}
                  >Passageiros</label>
                  <div className="choice-group-show" 
                        role="button"
                        aria-label="Passageiros"
                        onClick={() => {
                          this.setState(prevState => ({
                            passenger: {
                              ...prevState.passenger,
                              statusShowBox: true
                            }
                          }));
                          console.log(this.state);
                        }}
                  >
                    <div className="choice-group-show__name">{this.state.passenger.peopleTotal}</div>
                    <div className="choice-group-show__detail">
                      <span className="choice-group-show__detail__sig-airport">Pessoas</span>
                      <span className="choice-group-show__detail__airport">{this.state.passenger.cabin === 'EX' ? 'Classe executiva' : 'Classe econômica'}</span>
                    </div>
                    <i className="choice-group-show__icon fas fa-user-friends"></i>
                  </div>
                  <div className={"itens-passengers-choice "+ (this.state.passenger.statusShowBox === true ? 'show-box' : '') }>
                    <div className="dropdown-passengers">
                      <div className="col-12">
                        <span className="itens-passengers-choice__title">Passageiros e classe do voo</span>
                      </div>
                      <div className="passengers-info row">
                        <div className="col-4">
                          <div className="form-group">
                            <span className="passengers-info__label">Adultos <br />&nbsp;</span>
                            <select onChange={ (event) => {
                                      const valueTarget = event.target.value;
                                      let newObjectPassenger = this.state.passenger
                                      newObjectPassenger.adults = parseInt(valueTarget);
                                      newObjectPassenger.statusShowBox = true;
                                      this.setState({...this.state,
                                        passenger: newObjectPassenger
                                      })
                                    }} 
                                    value={this.state.passenger.adults} 
                                    name="adults" title="adultos" className="form-control" >
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                              <option>6</option>
                              <option>7</option>
                              <option>8</option>
                              <option>9</option>
                            </select>
                            <i className="icon-arrow-down form-control-feedback small"></i>
                          </div>
                        </div>
                        <div className="col-4">
                          <div className="form-group">
                            <span className="passengers-info__label">Crianças<br /><span className="age-range">2 a 11 anos</span></span>
                            <select onChange={ (event) => {
                                      const valueTarget = event.target.value;
                                      let newObjectPassenger = this.state.passenger
                                      newObjectPassenger.children = parseInt(valueTarget);
                                      newObjectPassenger.statusShowBox = true;

                                      this.setState({...this.state,
                                        passenger: newObjectPassenger
                                      })
                                    }} 
                                    value={this.state.passenger.children} 
                                    name="children" title="crianças" className="form-control">
                              <option>0</option>
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                              <option>6</option>
                              <option>7</option>
                              <option>8</option>
                              <option>9</option>
                            </select>
                            <i className="icon-arrow-down form-control-feedback small"></i>
                          </div>
                        </div>
                        <div className="col-4">
                          <div className="form-group ">
                            <span className="passengers-info__label">Bebês<br /><span className="age-range">0 a 23 meses</span></span>
                            <select onChange={ (event) => {
                                      const valueTarget = event.target.value;
                                      
                                      if(valueTarget > this.state.passenger.adults){
                                        this.createNotificationError('Número máximo de bebês é de 1 para cada adulto')
                                      } else {
                                        
                                        let newObjectPassenger = this.state.passenger
                                        newObjectPassenger.infants = parseInt(valueTarget);
                                        newObjectPassenger.statusShowBox = true;
                                        this.setState({...this.state,
                                          passenger: newObjectPassenger
                                        })
                                      }
                                    }} 
                                    value={this.state.passenger.infants} 
                                    name="infants" title="bebês" className="form-control">
                              <option value="0">0</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                            </select>
                            <i className="icon-arrow-down form-control-feedback small"></i>
                          </div>
                        </div>
                      </div>
                      <div className="col-12">
                        <div className="passengers-cabin">
                          <div className="form-group">
                            <span className="age-range">Classe do voo</span>
                            <div>
                              <select onChange={ (event) => {
                                      const valueTarget = event.target.value;
                                      let newObjectPassenger = this.state.passenger
                                      newObjectPassenger.cabin = valueTarget;
                                      newObjectPassenger.statusShowBox = true;
                                      this.setState({...this.state,
                                        passenger: newObjectPassenger
                                      })
                                    }} 
                                    value={this.state.passenger.cabin} 
                                    name="cabin" title="Classe do voo" className="form-control">
                                <option value="EC">Classe econômica</option>
                                <option value="EX">Classe executiva</option>
                              </select>
                              <i className="icon-arrow-down form-control-feedback small"></i>
                            </div>
                          </div>
                        </div>
                        <button type="button" 
                                className="btn btn-primary hidden-md hidden-lg"
                                onClick={() => {
                                  this.closeModalPassenger(this.state.passenger);
                                }}>
                          <i className="icon-arrow-right"></i>Confirmar
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              
              <div className="col-12">
                <button type="button" className="btn btn-primary" 
                        onClick={() => {this.runSearch()}}>
                  <i className="fa fa-search"></i> Pesquisar
                </button>
              </div>
            </div>
          </form>
        </section>
      
        <ResultSearch flightList={this.state.flightList}/>
      </React.Fragment>
    )
  }
}