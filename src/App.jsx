import React from 'react';
import 'bootstrap/scss/bootstrap.scss';
import '@fortawesome/fontawesome-free/css/all.css'
import './assets/scss/style.scss';
import Header from './components/Header';
import FormSearch from './components/form/FormSearch';


function App() {
  return (
    <div className="app">
      <Header />
      <FormSearch />
    </div>
  );
}

export default App;
